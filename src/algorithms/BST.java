package algorithms;

import main.BSTInterface;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Fine-Grained Locking Binary Tree
 * <p>
 * This class implements a concurrent version of a binary search tree.
 * <p>
 * This implementation insert + simple remove are based on the ideas from skip list
 * (http://people.csail.mit.edu/shanir/publications/Lazy_Concurrent.pdf)
 * java implementation from assignment 03.
 * <p>
 * node has 3 transitional states:
 * 1. removing - mark for removal but remove operation is not done yet.
 * 2. removed - node was removed from the list
 * 3. relocated - node was relocated due to complex remove operation, this state is needed since we are allocating
 * new node.
 * <p>
 * seek - is inspired by CASTLE BST (https://github.com/arunmoezhi/castle/blob/master/papers/techReport.pdf),
 * Seek finds node:
 * 1. node state != relocated -> return node.
 * 2. node state  == relocated -> retry.
 * Seek doesn't find node:
 * 1. last node of right turn (anchor) is under remove/removed -> retry seek.
 * 2. last node of right turn (anchor) has no op -> return null (not found)
 * <p>
 * Complex Remove - is adding flags to removed node + flags to relocated successor.
 * 1. acquire lock on targetParent, target and validating that they are not removed/relocated mark target as removing.
 * 2. seek successor, acquire lock on successorParent, successor + validate they are not removed/relocated.
 * 3. validate targetLeft, targetRight, successorChild (if present).
 * 4. set successorParent pointer to successorChild, mark successor as relocated.
 * 5. create new node with key == successor.key set left+right node. make targetParent point to node.
 * 6. mark target removed.
 *
 * @author Chen Dar <darc1>
 */
public class BST implements BSTInterface {

    private final Node root;

    public BST() {
        this.root = new RootNode();
    }

    public final boolean contains(final int key) {

        return seek(key).isFound();
    }

    public final boolean insert(final int key) {
        int iter = 0;
        while (true) {
//            System.out.println(MessageFormat.format("insert: {0}, iter: {1}", key, iter++));
            SeekResult seekResult = seek(key);

            if (seekResult.isFound()) {
                //node already in the tree
                return false;
            }

            //node not found, lock parent and insert.
            Node parent = seekResult.getParent();
            if (parent.tryLock()) {
                try {

                    if (notValid(parent)) continue;
                    //check that other thread didn't assign a child.
                    if (parent.hasChild(seekResult.getPosition())) continue;

                    Node node = new Node(key, 0, null);
                    parent.setChild(seekResult.getPosition(), node);

                    return true;

                } finally {
                    parent.unlock();
                }
            }
        }
    }


    public final boolean remove(final int key) {

        int iter = 0;
        while (true) {
//            System.out.println(MessageFormat.format("remove: {0}, iter: {1}", key, iter++));

            SeekResult seekResult = seek(key);

            //key not found
            if (!seekResult.isFound()) return false;

            //init remove state
            RemoveState state = new RemoveState(seekResult);

            try {

                //lock parent and verify it is not removed
                if (notLockedOrNotValid(state.tryLockTargetParent(), state.getTargetParent())) continue;
                //lock target and verify not removed
                if (notLockedOrNotValid(state.tryLockTarget(), state.getTarget())) continue;
                if (notParentAnymore(state.getTarget(), state.getTargetParent(), state.getTargetPosition())) continue;

                if (isSimpleRemove(state.getTarget())) {
                    //try simple remove
                    boolean success = attemptSimpleRemove(state);
                    if (!success) continue;
                    return true;

                } else {
                    //try complex remove
                    boolean success = attemptComplexRemove(state);
                    if (!success) continue;
                    return true;
                }

            } finally {
                //clear locks
                state.unlock();
            }
        }
    }

    private boolean notParentAnymore(Node child, Node parent, Position position) {
        return !parent.hasChild(position) || parent.getChild(position) != child;
    }


    private boolean attemptComplexRemove(RemoveState state) {

        SeekResult successorSeek = findMin(state.getTarget());
        if (!successorSeek.isFound()) return false;

        //add successor to remove state
        state.addSuccessor(successorSeek);

        if(!successorIsRight(state)){
            if (notLockedOrNotValid(state.tryLockSuccessorParent(), state.getSuccessorParent())) return false;
        }

        if (notLockedOrNotValid(state.tryLockSuccessor(), state.getSuccessor())) return false;

        SeekResult verifySuccessor = findMin(state.getTarget());
        if (verifySuccessor.getTarget() != state.getSuccessor()) return false;
        if (verifySuccessor.getParent() != state.getSuccessorParent()) return false;
        if (verifySuccessor.getPosition() != state.getSuccessorPosition()) return false;

        if (!isSimpleRemove(state.getSuccessor())) return false;

        if (state.getSuccessor().getLeft() != null) return false;

        if (state.getSuccessor().getRight() != null) {
            if (notValid(state.getSuccessor().getRight())) return false;
        }

        if (notValid(state.getTarget().getLeft())) return false;
        if (notValid(state.getTarget().getRight())) return false;

        //remove successor
        state.getSuccessor().setRelocated();
        state.getTarget().setRemoved();

        //create new node with successor value.
        Node newTarget = new Node(state.getSuccessor().getKey(), state.getTarget().getVersion() + 1, state.getTarget().getKey());
        synchronized (newTarget){
            newTarget.setLeft(state.getTarget().getLeft());
            if(successorIsRight(state)){
                newTarget.setRight(state.getSuccessor().getRight());
            }else{
                newTarget.setRight(state.getTarget().getRight());
            }
        }

        if (verifyNewTargetState(state, newTarget) && state.getTarget().isRemoved()) {
            state.getTargetParent().setChild(state.getTargetPosition(), newTarget);
        }

        if (!successorIsRight(state) && state.getSuccessor().isRelocated()) {
            state.getSuccessorParent().setChild(state.getSuccessorPosition(), state.getSuccessor().getRight());
        }

        return true;
    }

    private boolean successorIsRight(RemoveState state) {
        return state.getSuccessorPosition() == Position.RIGHT;
    }

    private boolean verifyNewTargetState(RemoveState state, Node newTarget) {
        boolean hasLeftChild = newTarget.hasChild(Position.LEFT);

        //checking right child is enough since we validate left child doesn't exist
        boolean successorHasChild = state.getSuccessor().getRight() != null;

        //check if successor is direct child of target
        boolean successorIsRightChild = successorIsRight(state);
        boolean successorIsRightChildWithoutChildren = successorIsRightChild && !successorHasChild;
        boolean hasRightChild = newTarget.hasChild(Position.RIGHT);

        //new target has no right child when successor is right child of target and has no children.
        //otherwise new target should have right child set.
        boolean hasRightChildOrRightChildIsSuccessor = successorIsRightChildWithoutChildren || hasRightChild;

        return hasLeftChild && hasRightChildOrRightChildIsSuccessor;
    }


    private boolean attemptSimpleRemove(RemoveState state) {

        Node targetOnlyChild = null;

        if (state.getTarget().hasChild(Position.RIGHT)) {
            if (notValid(state.getTarget().getRight())) return false;
            targetOnlyChild = state.getTarget().getRight();
        }

        if (state.getTarget().hasChild(Position.LEFT)) {
            if (notValid(state.getTarget().getLeft())) return false;
            targetOnlyChild = state.getTarget().getLeft();
        }

        state.getTarget().setRemoved();

        if (state.getTarget().isRemoved()) {
            state.getTargetParent().setChild(state.getTargetPosition(), targetOnlyChild);
        }

        return true;
    }

    private SeekResult seek2(final int key) {

        Node prevAnchor = null;
        while (true) {
            Node parent = root;
            Node anchor = root;
            Node curr = root.getRight();

            Position position = Position.RIGHT;

            while (curr != null) {

                if (curr.getKey() == key) {
                    break;
                }

                parent = curr;
                if (key < curr.getKey()) {
                    position = Position.LEFT;
                    curr = curr.getLeft();
                } else {
                    position = Position.RIGHT;
                    anchor = curr;
                    curr = curr.getRight();
                }
            }

            SeekResult seekResult = new SeekResult(parent, curr, position);

            if (curr == null) {
                //if no operation on last right turn then, tree state not changed return not found.
                if (!inTransitionState(anchor) && prevAnchor == anchor) {
                    return seekResult;

                    //last right turn is under operation, need to recheck
                } else {
                    prevAnchor = anchor;
                    continue;
                }
            }

            //if node has moved check again.
            if (curr.isRelocated()) {
                continue;
            }

            return seekResult;
        }

    }

    private SeekResult seek(final int key){

        int iter = 0;
        while(true){
//            System.out.println(MessageFormat.format("seek: {0}, iter: {1}", key, iter++));

            Node parent = root;
            Node anchor = root;
            Node curr = root.getRight();
            Position position = Position.RIGHT;

            if(curr == null) return new SeekResult(parent, curr, position);

            while(true){

                if(curr.getKey() == key) break;

                if(key < curr.getKey()){
                    position = Position.LEFT;
                }else{
                    position = Position.RIGHT;
                }

                Node tmp = curr.getChild(position);

                parent = curr;
                curr = tmp;

                if(tmp == null){
                    break;
                }

                if(position == Position.RIGHT){
                    anchor = parent;
                }
            }

            SeekResult seekResult = new SeekResult(parent, curr, position);
            if(curr != null && !curr.isRelocated()) return seekResult;

            if(curr == null && ((inTransitionState(anchor) ) || inTransitionState(parent))){
               continue;
            }

            return seekResult;
        }
    }

    private boolean inTransitionState(final Node node) {
        return node.isLocked() || notValid(node);
    }

    private boolean notLockedOrNotValid(final boolean isLocked, final Node node) {
        if (!isLocked) return true;
        if (notValid(node)) return true;
        return false;
    }

    private boolean notValid(final Node node) {
        if (node.isRemoved()) return true;
        if (node.isRelocated()) return true;
        return false;
    }

    private boolean isSimpleRemove(final Node target) {
        return target.getLeft() == null || target.getRight() == null;
    }

    // Return your ID #
    public String getName() {
        return "300921079";
    }

    // Returns size of the tree.
    public final int size() {
        // NOTE: Guaranteed to be called without concurrent operations,
        // so need to be thread-safe.  The method will only be called
        // once the benchmark completes.
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root.getRight());
        int count = 0;
        while (!queue.isEmpty()) {
            Node curr = queue.remove();
            count++;
            if (curr.getLeft() != null) {
                queue.add(curr.getLeft());
            }


            if (curr.getRight() != null) {
                queue.add(curr.getRight());
            }
        }
        return count;
    }

    // Returns the sum of keys in the tree
    public final long getKeysum() {
        // NOTE: Guaranteed to be called without concurrent operations,
        // so no need to be thread-safe.
        //
        // Make sure to sum over a "long" variable or you will get incorrect
        // results due to integer overflow!

//        uncomment to run validation that tree is bst
        validate();

        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root.getRight());
        long sum = 0;
        while (!queue.isEmpty()) {
            Node curr = queue.remove();
            sum += curr.getKey();
            if (curr.getLeft() != null) {
                queue.add(curr.getLeft());
            }


            if (curr.getRight() != null) {
                queue.add(curr.getRight());
            }
        }

        return sum;
    }

    private void validate() {

        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root.getRight());
        while (!queue.isEmpty()) {
            Node curr = queue.remove();
            if (curr.getLeft() != null) {
                if (curr.getLeft().getKey() >= curr.getKey()) {
                    throw new Error(MessageFormat.format("left key: {0} > curr key: {1}. \n curr: \n{2} \n left: \n{3}"
                            , curr.getLeft().getKey(), curr.getKey(), curr, curr.getLeft()));
                }
                queue.add(curr.getLeft());
            }


            if (curr.getRight() != null) {
                if (curr.getRight().getKey() <= curr.getKey()) {
                    throw new Error(MessageFormat.format("right key: {0} < curr key: {1}. \n curr: \n{2} \n right: \n{3}"
                            , curr.getRight().getKey(), curr.getKey(), curr, curr.getRight()));
                }

                queue.add(curr.getRight());
            }
        }

    }

    private SeekResult findMin(Node node) {

        Node parent = node;

        //curr can't be null since node is locked and we are in complex remove.
        Node curr = node.getRight();
        Position position = Position.RIGHT;

        while (curr.getLeft() != null) {

            Node tmp = curr.getLeft();
            //left node might have been removed.
            if (tmp == null) break;

            parent = curr;
            curr = tmp;
            position = Position.LEFT;
        }


        return new SeekResult(parent, curr, position);
    }

    class Node {

        final CASLock lock = new CASLock();
        private final int key;
        private final int version;
        protected Node left;
        protected Node right;
        private volatile boolean removed;
        private volatile boolean relocated;
        private final Integer prev;

        Node(final int key, final int version, final Integer prev) {
            this.key = key;
            this.version = version;
            this.prev = prev;
        }

        boolean tryLock() {
            return lock.tryLock();
        }

        void unlock() {
            lock.unlock();
        }

        boolean isLocked(){
            return lock.isLocked();
        }

        int getKey() {
            return key;
        }

        Node getLeft() {
            return left;
        }

        void setLeft(Node node) {
            validateLeft(node);
            this.left = node;
        }

        Node getRight() {

            return right;
        }

        void setRight(Node node) {

            validateRight(node);
            this.right = node;
        }

        private void validateLeft(Node node) {
//            if (node != null && node.getKey() >= this.getKey()) {
//                throw new Error(MessageFormat.format("new child key: {0} bigger/equal then this key: {1}\n\n new child: {2} \n\n this: {3} \n this right: {4} \n this left: {5} \n",
//                        node.getKey(), this.getKey(), node, this, this.getRight(), this.getLeft()));
//            }
//            if (node != null && (node.isRemoved() || node.isRelocated())) {
//                throw new Error(MessageFormat.format("invalid left node: \\n {0}\n\n this: {1} \n", node, this));
//            }

            if (node != null && node.getKey() >= this.getKey()) {
                System.out.println(MessageFormat.format("new child key: {0} bigger/equal then this key: {1}\n\n new child: {2} \n\n this: {3} \n this right: {4} \n this left: {5} \n",
                        node.getKey(), this.getKey(), node, this, this.getRight(), this.getLeft()));
                new Throwable().printStackTrace(System.out);
            }
            if (node != null && (node.isRemoved() || node.isRelocated())) {
                System.out.println(MessageFormat.format("invalid left node: \\n {0}\n\n this: {1} \n", node, this));
                new Throwable().printStackTrace(System.out);
            }
        }

        private void validateRight(Node node) {
//            if (node != null && node.getKey() <= this.getKey()) {
//                throw new Error(MessageFormat.format("new child key: {0} smaller/equal then this key: {1}\n\n new child: {2} \n\n this: {3} \n this right: {4} \n this left: {5} \n",
//                        node.getKey(), this.getKey(), node, this, this.getRight(), this.getLeft()));
//            }
//            if (node != null && (node.isRemoved() || node.isRelocated())) {
//                throw new Error(MessageFormat.format("invalid right node: \n {0}\n\n this: {1} \n ", node, this));
//            }

            if (node != null && node.getKey() <= this.getKey()) {
                System.out.println(MessageFormat.format("new child key: {0} smaller/equal then this key: {1}\n\n new child: {2} \n\n this: {3} \n this right: {4} \n this left: {5} \n",
                        node.getKey(), this.getKey(), node, this, this.getRight(), this.getLeft()));
                new Throwable().printStackTrace(System.out);
            }
            if (node != null && (node.isRemoved() || node.isRelocated())) {
                System.out.println(MessageFormat.format("invalid right node: \n {0}\n\n this: {1} \n ", node, this));
                new Throwable().printStackTrace(System.out);
            }
        }

        boolean isRemoved() {
            return removed;
        }

        void setRemoved() {
            this.removed = true;
        }

        void setChild(Position position, Node node) {
            if (position == Position.LEFT) {
                this.setLeft(node);
            } else {
                this.setRight(node);
            }
        }

        boolean hasChild(Position position) {
            if (position == Position.LEFT) {
                return this.getLeft() != null;
            } else {
                return this.getRight() != null;
            }
        }

        @Override
        public String toString() {
            return "{" +
                    "\n lock=" + lock +
                    "\n key=" + key +
                    "\n version=" + version +
                    "\n removed=" + removed +
                    "\n relocated=" + relocated +
                    "\n prev=" + prev +
                    '}';
        }

        boolean isRelocated() {
            return relocated;
        }

        void setRelocated() {
            this.relocated = true;
        }

        Node getChild(Position position) {
            if (position == Position.LEFT) {
                return this.getLeft();
            } else {
                return this.getRight();
            }
        }

        int getVersion() {
            return this.version;
        }
    }

    class RootNode extends Node{

        RootNode() {
            super(Integer.MIN_VALUE, -1, null);
        }

        @Override
        void setRight(Node node){
            this.right = node;
        }
    }

    class SeekResult {

        private final Node parent;
        private final Node target;
        private final Position position;

        SeekResult(final Node parent, final Node target, final Position position) {
            this.parent = parent;
            this.target = target;
            this.position = position;
        }

        Node getParent() {
            return parent;
        }

        Node getTarget() {
            return target;
        }

        Position getPosition() {
            return position;
        }

        boolean isFound() {
            return target != null && !target.isRemoved();
        }

        @Override
        public String toString() {
            return "SeekResult " +
                    "parent=" + parent.getKey() +
                    ", target=" + (target == null ? null : target.getKey()) +
                    ", position=" + position;
        }
    }

    enum Position {
        LEFT,
        RIGHT
    }

    class CASLock{

        final AtomicBoolean isLocked = new AtomicBoolean(false);

        boolean isLocked(){
            return isLocked.get();
        }

        boolean tryLock(){
            return isLocked.compareAndSet(false, true);
        }

        boolean unlock(){
            boolean isUnlocked = isLocked.compareAndSet(true, false);
            if(!isUnlocked) throw new Error("failed to unlock");
            return true;
        }

        @Override
        public String toString() {
            return "Lock{" +
                    "isLocked=" + isLocked +
                    '}';
        }
    }

    class RemoveState {

        final Node target;
        final Node targetParent;
        final Position targetPosition;

        Node successor;
        Node successorParent;
        Position successorPosition;

        boolean targetParentLocked = false;
        boolean targetLocked = false;
        boolean successorLocked = false;
        boolean successorParentLocked = false;


        RemoveState(final SeekResult result) {

            this.target = result.getTarget();
            this.targetParent = result.getParent();
            this.targetPosition = result.getPosition();
        }


        Node getTarget() {
            return target;
        }

        Node getTargetParent() {
            return targetParent;
        }

        Position getTargetPosition() {
            return targetPosition;
        }

        Node getSuccessor() {
            return successor;
        }

        Node getSuccessorParent() {
            return successorParent;
        }

        Position getSuccessorPosition() {
            return successorPosition;
        }

        void addSuccessor(final SeekResult seekResult) {
            this.successor = seekResult.getTarget();
            this.successorParent = seekResult.getParent();
            this.successorPosition = seekResult.getPosition();
        }

        boolean tryLockTarget() {
            targetLocked = tryLock(target);
            return targetLocked;
        }

        boolean tryLockTargetParent() {
            targetParentLocked = tryLock(targetParent);
            return targetParentLocked;
        }

        boolean tryLockSuccessor() {
            successorLocked = tryLock(successor);
            return successorLocked;
        }

        boolean tryLockSuccessorParent() {
            successorParentLocked = tryLock(successorParent);
            return successorParentLocked;
        }

        void unlock() {
            if (targetParentLocked) unlock(targetParent);
            if (targetLocked) unlock(target);
            if (successorParentLocked) unlock(successorParent);
            if (successorLocked) unlock(successor);
        }

        private boolean tryLock(Node node) {
            return node.tryLock();
        }

        private void unlock(Node node) {
            node.unlock();
        }

    }
}
